# Mythic dungeons
mythic_dungeons = [
    9028, # Atal'Dazar
    9164, # Freehold
    9526, # Kings' Rest
    9525, # Shrine of the Storm
    9354, # Siege of Boralus
    9527, # Temple of Sethraliss
    8064, # The MOTHERLODE!!
    9391, # The Underrot
    10043,# Tol Dagor [9327?]
    9424, # Waycrest Manor
]

# Map M+ keystone IDs (not the same as any other ID that identifies the instance of course) to [short, long] names
keystone_maps = {
    # Legion
    197: ['EoA', 'Eye of Azshara'],
    198: ['DHT', 'Darkheart Thicket'],
    199: ['BRH', 'Black Rook Hold'],
    200: ['HoV', 'Halls of Valor'],
    206: ['NL', 'Neltharion\'s Lair'],
    207: ['VoW', 'Vault of the Wardens'],
    208: ['Maw', 'Maw of Souls'],
    209: ['Arc', 'The Arcway'],
    210: ['CoS', 'Court of Stars'],
    227: ['RKL', 'Return to Karazhan: Lower'],
    233: ['CEN', 'Cathedral of Eternal Night'],
    234: ['RKU', 'Return to Karazhan: Upper'],
    239: ['SoT', 'Seat of the Triumvirate'],

    # Battle for Azeroth
    244: ['AD', "Atal'Dazar"],
    245: ['FH', 'Freehold'],
    246: ['TD', 'Tol Dagor'],
    247: ['TM!', 'THE MOTHERLODE!!'],
    248: ['WM', 'Waycrest Manor'],
    249: ['KR', "Kings' Rest"],
    250: ['ToS', 'Temple of Sethraliss'],
    251: ['TU', 'The Underrot'],
    252: ['SoS', 'Shrine of the Storm'],
    353: ['SoB', 'Siege of Boralus'], # Not a typo, it really is 353
}

keystone_order = [
    244, # Atal'Dazar
    245, # Freehold
    249, # Kings' Rest
    252, # Shrine of the Storm
    353, # Siege of Boralus
    250, # Temple of Sethraliss
    247, # The MOTHERLODE!!
    251, # The Underrot
    246, # Tol Dagor
    248, # Waycrest Manor
]

# Short difficulty names
difficulty_short = {
    3: '10 Normal', # Raid
    4: '25 Normal', # Raid
    5: '10 Heroic', # Raid
    6: '25 Heroic', # Raid
    7: 'LFR', # Raid
    14: 'Normal', # Raid
    15: 'Heroic', # Raid
    16: 'Mythic', # Raid
    17: 'LFR', # Raid
    23: 'Mythic', # Dungeon
}

# Split lockout instances - display as M-HoF, H-HoF, etc
split_lockouts = [
    # Mists of Pandaria
    6279, # Heart of Fear
    6125, # Mogu'shan Vaults
    6738, # Siege of Orgrimmar
    6067, # Terrace of Endless Spring
    6622, # Throne of Thunder

    # Warlords of Draenor
    6996, # Highmaul
    6967, # Blackrock Foundry
    7545, # Hellfire Citadel

    # Legion
    8026, # The Emerald Nightmare
    8440, # Trial of Valor
    8025, # The Nighthold
    8524, # Tomb of Sargeras
    8638, # Antorus, the Burning Throne

    # Battle for Azeroth
    9389, # Uldir
]

# Raid sort order override - these are sorted in descending order, so start at e9 for expansion e and go down
raid_order_override = {
    # Legion
    8026: 69, # The Emerald Nightmare
    8440: 68, # Trial of Valor
    8025: 67, # The Nighthold
    8524: 66, # Tomb of Sargeras
    8638: 65, # Antorus, the Burning Throne

    # Battle for Azeroth
    9389: 79, # Uldir
}
