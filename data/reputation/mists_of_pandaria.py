reputations_pandaria = dict(
    name='Mists of Pandaria',
    key='mop',
    reputations=[
        # 5.4 - Timeless Isle
        [
            dict(id=1492, name='Emperor Shaohao', icon='ability_monk_quipunch'),
        ],
        # 5.2 - Isle of Thunder
        [
            dict(
                alliance_id=1387,
                alliance_name='Kirin Tor Offensive',
                alliance_icon='achievement_reputation_kirintor_offensive',
                horde_id=1388,
                horde_name='Sunreaver Onslaught',
                horde_icon='achievement_faction_sunreaveronslaught',
            ),
            dict(id=1435, name='Shado-Pan Assault', icon='achievement_faction_shadopan'),
        ],
        # 5.1 - PvP stuff
        [
            dict(
                alliance_id=1376,
                alliance_name='Operation: Shieldwall',
                alliance_icon='achievement_general_allianceslayer',
                horde_id=1375,
                horde_name='Dominance Offensive',
                horde_icon='achievement_general_hordeslayer',
            ),
        ],
        [
            dict(id=1359, name='The Black Prince', icon='inv_legendary_theblackprince'),
            # Doesn't seem to be in API data
            # dict(
            #     alliance_id=1242,
            #     alliance_name='Pearlfin Jinyu',
            #     alliance_icon='inv_misc_fish_58',
            #     horde_id=1228,
            #     horde_name='Forest Hozen',
            #     horde_icon='inv_misc_fish_58',
            # ),
            dict(id=1302, name='The Anglers', icon='achievement_faction_anglers'),
            dict(id=1341, name='The August Celestials', icon='achievement_faction_celestials'),
            dict(id=1269, name='Golden Lotus', icon='achievement_faction_goldenlotus'),
            dict(id=1337, name='The Klaxxi', icon='achievement_faction_klaxxi'),
            dict(id=1345, name='The Lorewalkers', icon='achievement_faction_lorewalkers'),
            dict(id=1271, name='Order of the Cloud Serpent', icon='achievement_faction_serpentriders'),
            dict(id=1270, name='Shado-Pan', icon='achievement_faction_shadopan'),
            dict(id=1272, name='The Tillers', icon='achievement_faction_tillers'),
        ],
        [
            dict(id=1358, name='Nat Pagle', icon='inv_helmet_50', friend=True),
        ],
        # The Tillers
        [
            dict(id=1277, name='Chee Chee', icon='inv_misc_food_cooked_valleystirfry', note='Loves Valley Stir Fry', friend=True),
            dict(id=1275, name='Ella', icon='inv_misc_food_cooked_shrimpdumplings', note='Loves Shrimp Dumplings', friend=True),
            dict(id=1283, name='Farmer Fung', icon='inv_misc_food_cooked_wildfowlroast', note='Loves Wildfowl Roast', friend=True),
            dict(id=1282, name='Fish Fellreed', icon='inv_misc_food_cooked_twinfishplatter', note='Loves Twin Fish Platter', friend=True),
            dict(id=1281, name='Gina Mudclaw', icon='inv_misc_food_cooked_swirlingmistsoup', note='Loves Swirling Mist Soup', friend=True),
            dict(id=1279, name='Haohan Mudclaw', icon='inv_misc_food_cooked_tigersteak', note='Loves Charbroiled Tiger Steak', friend=True),
            dict(id=1273, name='Jogu the Drunk', icon='inv_misc_food_cooked_sauteedcarrots', note='Loves Sauteed Carrots', friend=True),
            dict(id=1276, name='Old Hillpaw', icon='inv_misc_food_cooked_braisedturtle', note='Loves Braised Turtle', friend=True),
            dict(id=1278, name='Sho', icon='inv_misc_food_cooked_eternalblossomfish', note='Loves Eternal Blossom Fish', friend=True),
            dict(id=1280, name='Tina Mudclaw', icon='inv_misc_food_cooked_firespiritsalmon', note='Loves Fire Spirit Salmon', friend=True),
        ],
    ],
)
