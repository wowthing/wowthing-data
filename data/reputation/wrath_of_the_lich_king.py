reputations_wrath = dict(
    name='Wrath of the Lich King',
    key='wotlk',
    reputations=[
        # 3.3 - Icecrown Citadel
        [
            dict(id=1156, name='The Ashen Verdict', icon='achievement_dungeon_icecrown_icecrownentrance'),
        ],
        [
            dict(id=1104, name='Frenzyheart Tribe', icon='achievement_zone_sholazar_08'),
            dict(id=1105, name='The Oracles', icon='achievement_zone_sholazar_11'),
        ],
        [
            dict(id=1106, name='Argent Crusade', icon='inv_jewelry_talisman_08'),
            dict(id=1073, name="The Kalu'ak", icon='achievement_reputation_tuskarr'),
            dict(id=1090, name='Kirin Tor', icon='achievement_reputation_kirintor'),
            dict(id=1098, name='Knights of the Ebon Blade', icon='achievement_reputation_knightsoftheebonblade'),
            dict(id=1119, name='The Sons of Hodir', icon='inv_misc_rune_14'),
            dict(id=1091, name='The Wyrmrest Accord', icon='achievement_reputation_wyrmresttemple'),
        ],
        [
            dict(
                alliance_id=1037,
                alliance_name='The Alliance Vanguard',
                alliance_icon='spell_misc_hellifrepvphonorholdfavor',
                horde_id=1052,
                horde_name='Horde Expedition',
                horde_icon='spell_misc_hellifrepvpthrallmarfavor',
            ),
            dict(
                alliance_id=1068,
                alliance_name="Explorers' League", # Dwarves, Howling Fjord/Storm Peaks
                alliance_icon='', # inv_icon_daily_mission_scroll ?
                horde_id=1067,
                horde_name='The Hand of Vengeance', # Forsaken, Howling Fjord/Dragonblight
                horde_icon='',
            ),
            dict(
                alliance_id=1126,
                alliance_name='The Frostborn', # Dwarves, Storm Peaks
                alliance_icon='',
                horde_id=1064,
                horde_name='The Taunka', # Tauren-ish, Borean Tundra/Howling Fjord
                horde_icon='',
            ),
            dict(
                alliance_id=1094,
                alliance_name='The Silver Covenant',
                alliance_icon='inv_elemental_primal_mana',
                horde_id=1124,
                horde_name='The Sunreavers',
                horde_icon='inv_elemental_primal_nether',
            ),
            dict(
                alliance_id=1050,
                alliance_name='Valiance Expedition',
                alliance_icon='',
                horde_id=1085,
                horde_name='The Warsong Offensive',
                horde_icon='',
            ),
        ],
    ],
)
